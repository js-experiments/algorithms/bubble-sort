const observable = factory => (...args) => {
  const observers = [];
  const obj = factory(...args);
  const notify = (event, ...args) =>
    observers
      .filter(o => o.event === event)
      .forEach(o => o.callback.apply(obj, [event, ...args]));

  return Object.assign(
    {
      subscribe(event, callback) {
        observers.push({
          event,
          callback,
        });
      },
      notify,
    },
    obj,
  );
};

const bubbleSort = observable(() => {
  return {
    list: [],
    add(n) {
      this.list.push(n);
      this.notify('add', n);
    },
    sort() {
      let ordered = [...this.list];
      let swapped = false;
      let length = ordered.length;
      do {
        swapped = false;
        for (let i = 1; i < length; i++) {
          this.notify('compare', ordered[i - 1], ordered[i]);
          if (ordered[i - 1] > ordered[i]) {
            this.notify('swap', ordered[i - 1], ordered[i]);
            const temp = ordered[i];
            ordered[i] = ordered[i - 1];
            ordered[i - 1] = temp;
            swapped = true;
          } else {
            this.notify('release', ordered[i - 1], ordered[i]);
          }
        }
        length -= 1;
      } while (swapped);

      this.list = [...ordered];
    },
  };
});

const randomArr = size => {
  const list = [];
  let count = 0;
  while (count < size) {
    let rand = Math.floor(Math.random() * size + 1);
    while (list.indexOf(rand) > -1) {
      rand = Math.floor(Math.random() * size + 1);
    }
    list.push(rand);
    count++;
  }

  return list;
};

const getTranslate = (str, axis = 'x') => {
  return str
    .split('translate3d')[1]
    .replace(/, /gi, ',')
    .split(' ')[0]
    .replace(/\(|\)/gi, '')
    .split(',')[axis === 'x' ? 0 : axis === 'y' ? 1 : 2];
};

const renderer = (sorter, toAdd, mainElm = null) => {
  // DOM Elements
  const mainElement = mainElm || document.getElementById('main');
  const speedElement = document.getElementById('speed');
  const algorithmContainer = document.createElement('section');
  const algorithmElement = document.createElement('div');

  let toAnimate = [];
  let intervalId;
  let speed = +speedElement.value;
  let nextAnimation;
  let added = 0;
  let swapStep;

  speedElement.addEventListener('change', () => {
    clearInterval(intervalId);
    speed = +speedElement.max + +speedElement.min - +speedElement.value;
    render(nextAnimation);
  });

  const onAdd = (type, value) => {
    toAnimate.push({
      type,
      values: [value],
    });
  };

  const onCompareSwap = (type, a, b) => {
    toAnimate.push({
      type,
      values: [a, b],
    });
  };

  const createValueElement = v => {
    const modifiers = [
      '--top',
      '--bottom',
      '--front',
      '--back',
      '--left',
      '--right',
    ];
    const mainElement = document.createElement('div');
    mainElement.classList.add('element', 'box', `value_${v}`);
    const faces = Array.from(Array(6), (item, index) => {
      const face = document.createElement('div');
      face.classList.add('box__face', `box__face${modifiers[index]}`);
      face.textContent = v;
      mainElement.appendChild(face);

      return face;
    });

    return mainElement;
  };

  const render = function(next) {
    nextAnimation = next;
    intervalId = setInterval(() => {
      const animation = toAnimate.shift();

      if (!animation) {
        clearInterval(intervalId);
        speedElement.disabled = false;
        if (next) {
          return next.call(this);
        }

        return;
      }

      const {values, type} = animation;
      const elements = values.map(
        v => document.querySelector(`.value_${v}`) || createValueElement(v),
      );

      switch (type) {
        case 'add':
          elements.forEach(e => {
            algorithmElement.appendChild(e);
            e.style.transform = `translate3d(${added *
              100}px, 0, 0) rotateX(0) rotateZ(0)`;
            added += 1;
          });
          break;
        case 'compare':
          elements.forEach(e => {
            const tX = getTranslate(e.style.transform);

            e.style.transform = `translate3d(${tX}, 0, 360px) rotateX(720deg) rotateZ(0)`;
          });
          break;
        case 'release':
          elements.forEach(e => {
            const tX = getTranslate(e.style.transform);

            e.style.transform = `translate3d(${tX}, 0, 0) rotateX(0) rotateZ(0)`;
            e.addEventListener('animationend', () => (e.style.animation = ''));
          });
          break;
        default:
          [a, b] = elements;
          const aX = getTranslate(a.style.transform);
          const bX = getTranslate(b.style.transform);
          const aZ = getTranslate(a.style.transform, 'z');
          const bZ = getTranslate(b.style.transform, 'z');

          clearInterval(intervalId);
          a.style.transform = `translate3d(${aX}, -100px, ${aZ}) rotateX(1080deg) rotateZ(0)`;
          b.style.transform = `translate3d(${bX}, 100px, ${bZ}) rotateX(360deg) rotateZ(0)`;

          let step = 1;
          swapStep = e => {
            if (step === 4) {
              a.removeEventListener('transitionend', swapStep);
              render(next);
            } else if (step === 1) {
              a.style.transform = `translate3d(${bX}, -100px, ${aZ}) rotateX(1080deg) rotateZ(720deg)`;
              b.style.transform = `translate3d(${aX}, 100px, ${bZ}) rotateX(360deg) rotateZ(-720deg)`;
            } else if (step === 2) {
              a.style.transform = `translate3d(${bX}, 0, ${aZ}) rotateX(1080deg) rotateZ(0)`;
              b.style.transform = `translate3d(${aX}, 0, ${bZ}) rotateX(360deg) rotateZ(0)`;
            } else {
              a.style.transform = `translate3d(${bX}, 0, 0) rotateX(0) rotateZ(0)`;
              b.style.transform = `translate3d(${aX}, 0, 0) rotateX(0) rotateZ(0)`;
            }
            step += 1;
          };
          a.addEventListener('transitionend', swapStep);
      }
    }, speed);
  };

  sorter.subscribe('add', (e, value) => onAdd(e, value));
  sorter.subscribe('compare', (e, a, b) => onCompareSwap(e, a, b));
  sorter.subscribe('release', (e, a, b) => onCompareSwap(e, a, b));
  sorter.subscribe('swap', (e, a, b) => onCompareSwap(e, a, b));

  return {
    renderAddSort() {
      toAdd.forEach(n => sorter.add(n));

      sorter.sort();

      return this;
    },

    start(next) {
      algorithmContainer.classList.add('algorithm');
      algorithmElement.classList.add('algorithm__elements');
      algorithmContainer.appendChild(algorithmElement);
      mainElement.appendChild(algorithmContainer);
      render.apply(this, [next]);

      // Clean list in an ugly way ;)
      sorter.list = [];
      speedElement.disabled = true;

      return this;
    },
    stop() {
      toAnimate = [];
      added = 0;
      clearInterval(intervalId);
      algorithmElement.innerHTML = '';
      algorithmElement.remove();
      algorithmContainer.innerHTML = '';
      algorithmContainer.remove();

      return this;
    },
  };
};

const arr = randomArr(10);

const algorithmRenderer = renderer(bubbleSort(), arr);

const menu = document.getElementById('algorithm-menu');
const menuItems = menu.querySelectorAll('li');
const onClickMenuItem = function(e) {
  const action = this.dataset.renderer;

  algorithmRenderer
    .stop()
    .start()
    [action]();
};

menuItems.forEach(item => item.addEventListener('click', onClickMenuItem));
